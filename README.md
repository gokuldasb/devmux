DevMux
======

[Tmux](https://github.com/tmux/tmux/wiki) session configuration for developers

[![pipeline status](https://gitlab.com/gokuldasb/devmux/badges/master/pipeline.svg)](https://gitlab.com/gokuldasb/devmux/commits/master)

DevMux allows you to launch predefined windows with specific functionality:  
- Home
- Servers
- Chat
- Develop
- Debug
- Python
- Embedded

DevMux creates a session of the same name and launches these windows. It
doesn't interfere with normal functioning of tmux. Unlike traditional session
managers like tmuxinator or tmuxp, DevMux doesn't allow you to define custom
session configurations. but comes withsome workflows that are difficult to
setup with traditional session managers.

## Installation

### Prerequisites
1. Zsh or Bash shell
2. Tmux
3. Python3
4. libtmux python library

### Optional Dependencies
1. Git (Needed only if you clone the application)
2. Htop (For system monitor)
3. ccze (For systemd journalctl log monitor)
4. Weechat (IRC and misc chat application)
5. Ranger (CLI file manager with vim key bindings)
6. Neovim (Editor)
7. gdb-dashboard (Improved interface for GDB)

## Usage
**TODO:** Usage instructions will be added as source is updated. The project is
currently under heavy development. Please take a look at the
[First Release Milestone](https://gitlab.com/gokuldasb/devmux/milestones/1) for
information about features under development and current status. All
contributions are welcome, especially from newbies.

## License
DevMux scripts and the rest of the contents of this repository are distributed
under the terms of MIT license. Refer 'LICENSE' file at the root of this source
tree for details.

Copyright © 2017 Gokul Das B
