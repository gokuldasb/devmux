# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Test for DevMux namer module

Use Nose2 to run this from root of source tree"""

import unittest
import libtmux as tmux
import devmux.common.namer as namer


class DevMuxTestSuite(unittest.TestCase):
    """Test suite for DevMux namer"""

    def setUp(self):
        server = tmux.Server()
        session = server.new_session('DevMux', attach=False)
        session.new_window('Main')
        session.new_window('Develop')
        session.new_window('Develop01')

    def tearDown(self):
        server = tmux.Server()
        server.kill_session('DevMux')

    def test_namer_name_sequence_fresh(self):
        """Test namer for fresh window name"""
        self.assertEqual(namer.give_name("Hello"), 'Hello')

    def test_namer_name_sequence_first(self):
        """Test namer for second copy of window name"""
        self.assertEqual(namer.give_name("Main"), 'Main01')

    def test_namer_name_sequence_second(self):
        """Test namer for third copy of window name"""
        self.assertEqual(namer.give_name("Develop"), 'Develop02')
