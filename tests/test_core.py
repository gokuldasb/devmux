# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Test for DevMux core module

Use Nose2 to run this from root of source tree"""

import unittest
import libtmux as tmux
import devmux.common.core as core


class DevMuxTestSuite(unittest.TestCase):
    """Test suite for DevMux"""

    def test_core_get_session_not_available(self):
        """Test get_session to return None when session not available"""
        server = tmux.Server()
        if server.has_session('DevMux'):
            server.kill_session('DevMux')
        self.assertIsNone(core.get_session())

    def test_core_get_session_available(self):
        """Test get_session to return session handle when available"""
        server = tmux.Server()
        if not server.has_session('DevMux'):
            server.new_session('DevMux', attach=False)
        self.assertIsNotNone(core.get_session())
