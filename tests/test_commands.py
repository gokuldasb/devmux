# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Test for DevMux commands module

Use Nose2 to run this from root of source tree"""

import unittest
import libtmux as tmux
from devmux.common.commands import process


class DevMuxTestSuite(unittest.TestCase):
    """Test suite for DevMux configuration"""

    def setUp(self):
        server = tmux.Server()
        self.had_session = True
        if not server.has_session('DevMux'):
            server.new_session('DevMux', attach=False)
            self.had_session = False

    def tearDown(self):
        if not self.had_session:
            server = tmux.Server()
            server.kill_session('DevMux')

    def test_process_cmd_0(self):
        """Test command processor for zero commands"""
        self.command = None
        self.args = ''
        process(["devmux"], ts=self)

    def test_process_cmd_1(self):
        """Test command processor for zero commands"""
        self.command = 'launch'
        self.args = ''
        process(["devmux", "launch"], ts=self)

    def test_process_args_1(self):
        """Test command processor with arguments"""
        self.command = 'init'
        self.args = 'foobar'
        process(["devmux", "init", "foobar"], ts=self)

    def test_process_window(self):
        """Test command processor for window build invocation"""
        self.command = 'home'
        self.args = 'foobar'
        process(["devmux", "home", "foobar"], ts=self)

    def test_process_invalid(self):
        """Test command processor for invalid commands"""
        with self.assertRaises(RuntimeError):
            process(["devmux", "foobar"], ts=self)
