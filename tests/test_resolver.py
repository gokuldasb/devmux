# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Tests for DevMux path resolver

Use Nose2 to run this from root of source tree"""

import unittest
import subprocess as sp
import os.path as path
import devmux.common.resolver as resolver


class DevMuxTestSuite(unittest.TestCase):
    """Test suite for DevMux path resolver"""

    def test_resolver_current_directory(self):
        """Test expansion of empty string"""
        work_dir = sp.run(['pwd'], stdout=sp.PIPE)
        work_dir = work_dir.stdout.decode('utf-8').strip()
        self.assertEqual(resolver.resolve_path(''), work_dir)

    def test_resolver_relative_path(self):
        """Test resolution of relative path"""
        work_dir = sp.run(['pwd'], stdout=sp.PIPE)
        work_dir = work_dir.stdout.decode('utf-8').strip()
        work_dir += '/foo'
        self.assertEqual(resolver.resolve_path('foo'), work_dir)

    def test_resolver_variable_expansion(self):
        """Test variable expansion"""
        home_dir = path.expandvars('$HOME') + '/foo'
        self.assertEqual(resolver.resolve_path('$HOME/foo'), home_dir)

    def test_resolver_home(self):
        """Test expansion of home directory"""
        home_dir = path.expandvars('$HOME') + '/foo'
        self.assertEqual(resolver.resolve_path('~/foo'), home_dir)

    def test_resolver_absolute_path(self):
        """Test expansion of absolute path"""
        work_dir = sp.run(['pwd'], stdout=sp.PIPE)
        work_dir = work_dir.stdout.decode('utf-8').strip()
        self.assertEqual(resolver.resolve_path(work_dir), work_dir)
