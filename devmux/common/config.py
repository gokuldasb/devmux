# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Configuration manager for DevMux"""

import toml
import os
from . import resolver


class Config:
    """DevMux Configuration Class"""

    @staticmethod
    def default_file_path():
        """Specify default path for devmux.toml"""
        conf_path = os.getenv('XDG_CONFIG_HOME',
                              os.path.expandvars('$HOME/.config'))
        return conf_path + '/devmux/devmux.toml'

    def __init__(self, path=None):
        """Loader and parser for TOML onfiguration file"""
        if path is None:
            path = self.default_file_path()
        else:
            path = resolver.resolve_path(path)
        if not os.path.exists(path):
            raise FileNotFoundError(path)
        configs = toml.load(path)
        self.dependencies = configs['Dependencies']
        self.windows = configs['Session-Configuration']['windows']
        srv_cmds = configs['Server-Commands']
        self.server_commands = [i['commands'] for i in srv_cmds]
