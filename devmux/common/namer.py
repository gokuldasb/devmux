# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Window namer for DevMux"""

# Strategy:
#     1. Take a name as input
#     2. Check if any window exists with that name as prefix
#     2A. If it does exist, name it NAME## (## is sequence number)
#     2B. If it doesn't exist, give it the requested name

from . import core


def name_sequence(name, number):
    """Set name based on sequence number"""
    if number == 0:
        return name
    else:
        return "{}{:02d}".format(name, number % 100)


def give_name(name):
    """Give numbered window name based on availability"""
    session = core.get_session()
    if session is None:
        raise RuntimeError("DevMux session doesn't exist")
    else:
        # window.get(key). Keys obtained from window.keys()
        windows = [i.get('window_name') for i in session.list_windows()]
        num = 0
        while name_sequence(name, num) in windows:
            num += 1
        return name_sequence(name, num)
