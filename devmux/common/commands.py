# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""Command processor for dmux invocation"""

import pkgutil
import os.path as path
from importlib.machinery import SourceFileLoader
from .config import Config
from . import core
from . import namer


def process(argv, ts=None):
    """Command procesor for DevMux"""

    # 1. Separate out arguments
    if len(argv) > 1:
        command = argv[1]
    else:
        command = None
    if len(argv) > 2:
        args = ' '.join(argv[2:])
    else:
        args = ''

    # 2. Load configuration
    config = Config()

    # 3. Invoke core commands with configuration and arguments
    if command is None:
        core.launch(args, config)
        if ts is not None:
            ts.assertIsNone(command)
            ts.assertEqual(args, ts.args)
        return
    if command == 'launch':
        core.launch(args, config)
        if ts is not None:
            ts.assertEqual(command, ts.command)
            ts.assertEqual(args, ts.args)
        return
    elif command == 'init':
        core.init(args, config)
        if ts is not None:
            ts.assertEqual(command, ts.command)
            ts.assertEqual(args, ts.args)
        return
    elif command == 'attach':
        core.attach(args, config)
        return
    elif core.get_session() is None:
        raise RuntimeError('Error: DevMux session missing')
        return

    # 4. Register window plugins
    winpkg = path.abspath(path.dirname(__file__) + '/../windows')
    modules = [i for i in pkgutil.iter_modules([winpkg]) if not i.ispkg]
    register = dict()
    for mod in modules:
        modpath = winpkg + '/' + mod.name + '.py'
        module = SourceFileLoader(mod.name, modpath).load_module()
        cmd, record = module.register()
        register[cmd] = record

    # 5. Invoke window loader with configuration and arguments
    if command in register:
        name = namer.give_name(register[command]['name'])
        register[command]['factory'].build(args, config, name)
        if ts is not None:
            ts.assertEqual(command, ts.command)
            ts.assertEqual(args, ts.args)
    else:
        raise RuntimeError('Error: Invalid command')
