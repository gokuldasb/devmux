# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""DevMux home window"""


class HomeFactory:
    """Home window factory"""

    @staticmethod
    def build(args, config, name):
        print("Building home window: ", name)


def register():
    """Register home window"""
    return ('home', {'name': 'Home', 'factory': HomeFactory()})
