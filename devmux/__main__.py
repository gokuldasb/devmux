# Copyright (c) 2017 Gokul Das B
# This source file is part of DevMux project and is under MIT License
# Refer 'LICENSE' file at the root of the source tree for details

"""DevMux script entry point"""

import sys
from common.commands import process

process(sys.argv)
